// Hide banner top
$(document).ready(function(){
    $("#close-banner-top").click(function(){
        $(".banner-top").slideToggle( "slow" );
    });
});

// Scroll to Top  
$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        
        $('#return-to-top').fadeIn(200);  
    } else {
        $('#return-to-top').fadeOut(200);   
    }
});
$('#return-to-top').click(function() {      
    $('body,html').animate({
        scrollTop : 0                       
    }, 500);
});

// Hide banner top
$(document).ready(function(){
    $(".btn-like-produk").click(function(){
        var $el = $(this);
        $el.toggleClass('like');
        $el.addClass('eff');
        window.setTimeout(function() {
            $el.removeClass('eff');
        }, 200);
    });
});

// Header Scroll
$(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    var objectSelect = $(".nav-fix-pos");
    var objectPosition = objectSelect.offset().top;
    if (scroll > objectPosition) {
       $("header").addClass("sticky");
    } else {
        $("header").removeClass("sticky");
    }
});

$(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    var objectSelect = $("#nav-fix-pos-home");
    var objectPosition = objectSelect.offset().top;
    if (scroll < objectPosition) {
       $(".select-kategori .kategori-dropdown").removeClass("d-block");
        $("body").removeClass("overlay-black");
    } else {
        
    }
});


// Dropdown Kategori
$(function() {
    $(".btn-tog-kategori").on("click", function(e) {
        $(".select-kategori .kategori-dropdown").toggleClass("d-block");
        $("body").toggleClass("overlay-black");
    });
    $(document).on("click", function(e) {
        if ($(e.target).is(".btn-tog-kategori, .kategori-dropdown") === false) {
            $(".select-kategori .kategori-dropdown").removeClass("d-block");
            $("body").removeClass("overlay-black");
        }else{
        // $(".ham-menu").addClass("change");
        }
    });
});






$('.banner-home-slide').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});

$('.produk-list-slide').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    dots:false,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:2
        },
        1000:{
            items:4
        }
    }
});

$('.patner').owlCarousel({
    loop:true,
    margin:30,
    nav:false,
    dots:false,
    responsive:{
        0:{
            items:3
        },
        600:{
            items:4
        },
        1000:{
            items:6
        }
    }
});
