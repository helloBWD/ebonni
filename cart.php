    <?php include 'header.php' ?>

    	<div class="page-content">
			<div class="container">
				<div class="text-center">
					<h1 class="mb-4">Cart</h1>
				</div>
				<div class="page-content-sec my-5">
					<div class="row">
						<div class="col-lg-9">
							<div class="cart-table">
								<div class="cart-table-head">
									<div class="cart-table-row">
										<div class="cart-table-item f-barang">
											Barang
										</div>
										<div class="cart-table-item f-harga">
											Harga
										</div>
										<div class="cart-table-item f-jumlah">
											Jumlah
										</div>
										<div class="cart-table-item f-subtotal">
											Subtotal
										</div>
									</div>
								</div>
								<div class="cart-table-body">
									<div class="cart-table-row">
										<div class="cart-table-item f-barang">
											<div class="cart-table-item-wrap">
												<img src="bwdassets/images/produk-1.png" class="img-fluid cart-item image">
												<div>
													<a href="#" class="cart-item name">Souvenir Tempat Pulpen Miniatur Kapal Nusantara</a>
													<!-- Mobile Harga -->
														<span class="cart-item harga mt-2 d-lg-none">
															Rp100.000
														</span>
													<!-- Mobile Harga -->	
												</div>											
											</div>										
										</div>
										<div class="cart-table-item f-harga">
											<div class="cart-table-item-wrap">
												<span class="cart-item harga">
													Rp100.000
												</span>
											</div>
										</div>
										<div class="cart-table-item f-jumlah">
											<div class="cart-table-item-wrap">
												<div class="input-group cart-item add-minus">
													<div class="input-group-prepend">
														<span class="input-group-text">-</span>
													</div>
													<input type="text" class="form-control font-weight-bold text-center" value="5">
													<div class="input-group-append">
														<span class="input-group-text">+</span>
													</div>
												</div>
											</div>
										</div>
										<div class="cart-table-item f-subtotal">
											<div class="cart-table-item-wrap">
												<span class="cart-item subharga">
													Rp120.000
												</span>
												<i class="fas fa-trash-alt cart-item remove"></i>
											</div>
										</div>	
									</div>
									<div class="cart-table-row">
										<div class="cart-table-item f-barang">
											<div class="cart-table-item-wrap">
												<img src="bwdassets/images/produk-1.png" class="img-fluid cart-item image">
												<div>
													<a href="#" class="cart-item name">Souvenir Tempat Pulpen Miniatur Kapal Nusantara</a>
													<!-- Mobile Harga -->
														<span class="cart-item harga mt-2 d-lg-none">
															Rp100.000
														</span>
													<!-- Mobile Harga -->	
												</div>											
											</div>										
										</div>
										<div class="cart-table-item f-harga">
											<div class="cart-table-item-wrap">
												<span class="cart-item harga">
													Rp100.000
												</span>
											</div>
										</div>
										<div class="cart-table-item f-jumlah">
											<div class="cart-table-item-wrap">
												<div class="input-group cart-item add-minus">
													<div class="input-group-prepend">
														<span class="input-group-text">-</span>
													</div>
													<input type="text" class="form-control font-weight-bold text-center" value="5">
													<div class="input-group-append">
														<span class="input-group-text">+</span>
													</div>
												</div>
											</div>
										</div>
										<div class="cart-table-item f-subtotal">
											<div class="cart-table-item-wrap">
												<span class="cart-item subharga">
													Rp120.000
												</span>
												<i class="fas fa-trash-alt cart-item remove"></i>
											</div>
										</div>	
									</div>
									<div class="cart-table-row">
										<div class="cart-table-item f-barang">
											<div class="cart-table-item-wrap">
												<img src="bwdassets/images/produk-1.png" class="img-fluid cart-item image">
												<div>
													<a href="#" class="cart-item name">Souvenir Tempat Pulpen Miniatur Kapal Nusantara</a>
													<!-- Mobile Harga -->
														<span class="cart-item harga mt-2 d-lg-none">
															Rp100.000
														</span>
													<!-- Mobile Harga -->	
												</div>											
											</div>										
										</div>
										<div class="cart-table-item f-harga">
											<div class="cart-table-item-wrap">
												<span class="cart-item harga">
													Rp100.000
												</span>
											</div>
										</div>
										<div class="cart-table-item f-jumlah">
											<div class="cart-table-item-wrap">
												<div class="input-group cart-item add-minus">
													<div class="input-group-prepend">
														<span class="input-group-text">-</span>
													</div>
													<input type="text" class="form-control font-weight-bold text-center" value="5">
													<div class="input-group-append">
														<span class="input-group-text">+</span>
													</div>
												</div>
											</div>
										</div>
										<div class="cart-table-item f-subtotal">
											<div class="cart-table-item-wrap">
												<span class="cart-item subharga">
													Rp120.000
												</span>
												<i class="fas fa-trash-alt cart-item remove"></i>
											</div>
										</div>	
									</div>				
								</div>
							</div>
						</div>
						<div class="col-lg-3 mt-3 mt-lg-0">
							<div class="card border-0 rounded-0">
								<div class="card-body  pt-2">
									<table class="cart-table-total mb-md-3">
										<tr>
											<td class="label">Total Barang</td>										
											<td class="harga">Rp100.000</td>
										</tr>
										<tr>
											<td class="label">Diskon</td>										
											<td class="harga">Rp50.000</td>
										</tr>
										<tr class="total-box d-none d-md-table-row">
											<td class="label">Total</td>										
											<td class="harga">Rp50.000</td>
										</tr>
									</table>	
									<div class="text-center d-none d-md-block">
										<a href="checkout.php" class="btn btn-orange w-100 rounded-0">Lanjutkan Checkout</a>
									</div>								
								</div>							
							</div>	
							<!-- Mobile Total -->
								<div class="card cart-box-total border-0 rounded-0 d-md-none">
									<div class="card-body">
										<table class="cart-table-total mb-3">
											<tr class="total-box">
												<td class="label border-0 py-0">Total</td>										
												<td class="harga border-0 py-0">Rp50.000</td>
											</tr>
										</table>	
										<div class="text-center">
											<a href="checkout.php" class="btn btn-orange w-100 rounded-0">Lanjutkan Checkout</a>
										</div>								
									</div>							
								</div>
							<!-- End Mobile Total -->					
						</div>
					</div>	
				</div>								
			</div>
		</div>

    <?php include 'footer.php' ?>

    						