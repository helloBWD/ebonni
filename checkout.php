<?php include 'header-without-nav.php' ?>

	<nav class="navbar navbar-light navbar-checkout bg-white">
		<div class="container">
			<a class="navbar-brand" href="#">
				<img src="bwdassets/images/logo-text.png" height="30" alt="">
			</a>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					Pembayaran
				</li>
			</ul>
		</div>
		
	</nav>

	<div class="py-5">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="card rounded-0">
						<div class="card-header">
							<span class="text-16 font-weight-bold">Detail Pembeli</span>
						</div>
						<div class="card-body">
							<ul class="nav nav-tabs checkout-tabs" id="myTab" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="login-tab" data-toggle="tab" href="#login" role="tab" aria-controls="login" aria-selected="true">Login</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="balitd-tab" data-toggle="tab" href="#belitd" role="tab" aria-controls="belitd" aria-selected="false">Beli Tanpa Daftar</a>
								</li>
							</ul>
							<div class="tab-content py-4" id="myTabContent">
								<div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
									<form>
		                                <div class="form-group">
		                                	<label>Email</label>
		                                    <input type="email" class="form-control">
		                                </div>
		                                <div class="form-group">
		                                	<label>Password</label>
		                                    <input type="password" class="form-control">
		                                </div>     
		                                <div>
		                                    <a href="#" class="text-orange mt-1">Lupa Password? </a>
		                                </div>                           
		                                <button type="submit" class="btn btn-orange w-100 rounded-0 font-weight-bold mt-2 float-right">Login</button>
		                            </form>
								</div>
								<div class="tab-pane fade" id="belitd" role="tabpanel" aria-labelledby="balitd-tab">
									<form>
										<div class="row">
											<div class="col-12">
												<div class="form-group">
				                                	<label>Nama Pembeli</label>
				                                    <input type="text" class="form-control">
				                                </div>
											</div>
											<div class="col-6">
												<div class="form-group">
				                                	<label>E-mail Pembeli</label>
				                                    <input type="text" class="form-control">
				                                </div>
											</div>
											<div class="col-6">
												<div class="form-group">
				                                	<label>Telepon/Handphone</label>
				                                    <input type="number" class="form-control">
				                                </div>
											</div>
											<div class="col-6">
												<a href="#" class="btn btn-submit w-100 rounded-0"  data-toggle="modal" data-target="#alamat-modal"> Masukan Alamat Pengiriman </a>
											</div>
										</div>
		                            </form>
								</div>
							</div>
						</div>
					</div>
					<div class="card rounded-0 mt-4">
						<div class="card-header">
							<span class="text-16 font-weight-bold">Detail Belanja</span>
						</div>
						<div class="card-body">
							<div class="card rounded-0">
								<div class="card-header d-flex align-items-center bg-transparent">
									<img src="bwdassets/images/shop.svg" width="20px" class="mr-2"><span>Giri Pustaka</span>
								</div>
								<div class="card-body">
									<div class="cart-table">
										<div class="cart-table-body">
											<div class="cart-table-row">
												<div class="cart-table-item f-barang">
													<div class="cart-table-item-wrap">
														<img src="bwdassets/images/produk-1.png" class="img-fluid cart-item image">
														<div>
															<a href="#" class="cart-item name">Souvenir Tempat Pulpen Miniatur Kapal Nusantara</a>
															<!-- Mobile Harga -->
																<span class="cart-item harga mt-2 d-lg-none">
																	Rp100.000
																</span>
															<!-- Mobile Harga -->	
														</div>											
													</div>										
												</div>
												<div class="cart-table-item f-harga">
													<div class="cart-table-item-wrap">
														<span class="cart-item harga">
															Rp100.000
														</span>
													</div>
												</div>
												<div class="cart-table-item f-jumlah">
													<div class="cart-table-item-wrap">
														<div class="input-group cart-item add-minus">
															<div class="input-group-prepend">
																<span class="input-group-text">-</span>
															</div>
															<input type="text" class="form-control font-weight-bold text-center" value="5">
															<div class="input-group-append">
																<span class="input-group-text">+</span>
															</div>
														</div>
													</div>
												</div>
												<div class="cart-table-item f-subtotal">
													<div class="cart-table-item-wrap">
														<span class="cart-item subharga">
															Rp120.000
														</span>
														<i class="fas fa-trash-alt cart-item remove"></i>
													</div>
												</div>	
											</div>
											<div class="cart-table-row">
												<div class="cart-table-item f-barang">
													<div class="cart-table-item-wrap">
														<img src="bwdassets/images/produk-1.png" class="img-fluid cart-item image">
														<div>
															<a href="#" class="cart-item name">Souvenir Tempat Pulpen Miniatur Kapal Nusantara</a>
															<!-- Mobile Harga -->
																<span class="cart-item harga mt-2 d-lg-none">
																	Rp100.000
																</span>
															<!-- Mobile Harga -->	
														</div>											
													</div>										
												</div>
												<div class="cart-table-item f-harga">
													<div class="cart-table-item-wrap">
														<span class="cart-item harga">
															Rp100.000
														</span>
													</div>
												</div>
												<div class="cart-table-item f-jumlah">
													<div class="cart-table-item-wrap">
														<div class="input-group cart-item add-minus">
															<div class="input-group-prepend">
																<span class="input-group-text">-</span>
															</div>
															<input type="text" class="form-control font-weight-bold text-center" value="5">
															<div class="input-group-append">
																<span class="input-group-text">+</span>
															</div>
														</div>
													</div>
												</div>
												<div class="cart-table-item f-subtotal">
													<div class="cart-table-item-wrap">
														<span class="cart-item subharga">
															Rp120.000
														</span>
														<i class="fas fa-trash-alt cart-item remove"></i>
													</div>
												</div>	
											</div>				
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mt-4 mt-lg-0">
					<div class="card rounded-0">
						<div class="card-header">
							<span class="text-16 font-weight-bold">Pilih Kurir</span>
						</div>
						<div class="card-body">
							<form>
                                <div class="form-group">
                                	<label>Kurir</label>
                                    <select class="form-control">
										<option>JNE REG (6 Hari kerja)</option>
										<option>POS KILAT (3 Hari kerja)</option>
									</select>
                                </div>
                                <div class="form-group">
                                	<label>Catatan</label>
                                    <textarea class="form-control" rows="4"></textarea>
                                </div> 
                                <div class="form-group">
                                	<label>Biaya Kirim</label>
                                    <input type="text" class="form-control bg-white font-weight-bold" readonly="" value="Rp50.000">
                                </div>                         
                            </form>
						</div>
					</div>
					<div class="card rounded-0 mt-4">
						<div class="card-header">
							<span class="text-16 font-weight-bold">Ringkasan Belanja</span>
						</div>
						<div class="card-body">
							<table class="cart-table-total mb-md-3">
								<tr>
									<td class="label">Total Harga Barang</td>										
									<td class="harga">Rp100.000</td>
								</tr>
								<tr>
									<td class="label">Biaya Kirim</td>										
									<td class="harga">Rp50.000</td>
								</tr>
								<tr class="total-box d-none d-md-table-row">
									<td class="label">Total</td>										
									<td class="harga">Rp150.000</td>
								</tr>
							</table>	
							<div class="text-center d-none d-md-block">
								<a href="invoice.php" class="btn btn-orange w-100 font-weight-bold rounded-0">Pembayaran</a>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Alamat -->
		<div class="modal fade" id="alamat-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centeorange" role="document">
				<div class="modal-content">
					<div class="card rounded-0">
						<div class="card-header">
							<span class="text-16 font-weight-bold">Alamat Pengiriman</span>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="card-body">
							<form>
	                            <div class="form-group">
	                            	<label>Provinsi</label>
									<select class="form-control">
										<option>Jawa Timur</option>
										<option>Jawa Tengah</option>
									</select>
	                            </div>
	                            <div class="form-group">
	                            	<label>Kota/Kabupaten</label>
	                                <select class="form-control">
										<option>Malang</option>
										<option>Kediri</option>
									</select>
	                            </div>   
	                            <div class="form-group">
	                            	<label>Kecamatan</label>
	                                <select class="form-control">
										<option>Ngantang</option>
										<option>Ngaglik</option>
									</select>
	                            </div> 
	                            <div class="form-group">
	                            	<label>Alamat lengkap</label>
	                                <textarea class="form-control" rows="4"></textarea>
	                            </div>                            
	                            <button type="submit" class="btn btn-orange w-100 rounded-0 font-weight-bold mt-2 float-right">Simpan</button>
	                        </form>
						</div>
					</div>
				</div>
			</div>
		</div>
	<!-- End Modal Alamat -->

<?php include 'footer-without-nav.php' ?>