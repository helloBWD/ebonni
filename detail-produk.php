<?php include 'header-without-banner-top.php' ?>

	<div class="page-content pt-0">
		<div class="container">
			<div>
			<nav aria-label="breadcrumb ">
				<ol class="breadcrumb my-0 py-4">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Food</a></li>
                    <li class="breadcrumb-item"><a href="#">Bahan Mentah</a></li>
					<li class="breadcrumb-item active" aria-current="page">Beras Punel Barito merek ViO</li>
				</ol>
			</nav>
			</div>
			<div class="row">
				<div class="col-lg-9">
					<div class="card border-0 rounded-0">
						<div class="card-body pt-4 pb-5">
							<div class="row">
								<div class="col-md-4 mb-3">
                                    <div class="produk-detail-slide">
                                        <div><img src="bwdassets/images/produk-detail.png" class="w-100"></div>
                                        <div><img src="bwdassets/images/produk-detail.png" class="w-100"></div>
                                        <div><img src="bwdassets/images/produk-detail.png" class="w-100"></div>
                                        <div><img src="bwdassets/images/produk-detail.png" class="w-100"></div>
                                        <div><img src="bwdassets/images/produk-detail.png" class="w-100"></div>
                                    </div>
                                    <div class="produk-detail-slide-nav">
                                        <div><img src="bwdassets/images/produk-detail.png" class="img-fluid px-2"></div>
                                        <div><img src="bwdassets/images/produk-detail.png" class="img-fluid px-2"></div>
                                        <div><img src="bwdassets/images/produk-detail.png" class="img-fluid px-2"></div>
                                        <div><img src="bwdassets/images/produk-detail.png" class="img-fluid px-2"></div>
                                        <div><img src="bwdassets/images/produk-detail.png" class="img-fluid px-2"></div>
                                    </div>
								</div>
								<div class="col-md-8">
									<h1 class="produk-nama">Beras Punel Barito Merek ViO</h1>
									<hr>
									<div class="mb-1">
                                        <ul class="rating-list float-left mb-0 mr-2">
                                            <li class="active"><i class="fas fa-star"></i></li>
                                            <li class="active"><i class="fas fa-star"></i></li>
                                            <li class="active"><i class="fas fa-star"></i></li>
                                            <li class=""><i class="fas fa-star"></i></li>
                                        </ul>
                                        <small class="text-grey-soft mr-3">294 Ulasan</small>
                                        <img src="bwdassets/images/kualitas-a.png" class="img-fluid mr-1">
                                        <span class="text-grey">Kualitas : A</span>
                                    </div> 
                                    <div class="produk-harga mt-4">
                                    	<span>Rp10.000</span>&nbsp;<span class="satuan">/ kg</span>
                                    </div>
                                    <div class="produk-notice my-4">
                                    	Minimal transaksi <span>5 Ton</span>
                                    </div>
                                    <div class="table-produk-detail">
                                       	<div class="row ">
                                    		<div class="col-md-4">
                                    			<label>Merk Dagang</label>
                                    		</div>
                                    		<div class="col-md-8">
                                    			Adruino
                                    		</div>
                                    	</div>
                                    	<div class="row">
                                    		<div class="col-md-4">
                                    			<label>Stok Barang</label>
                                    		</div>
                                    		<div class="col-md-8">
                                    			1000 karung
                                    		</div>
                                    	</div>
                                    	<div class="row">
                                    		<div class="col-md-4">
                                    			<label>Deskripsi Kualitas</label>
                                    		</div>
                                    		<div class="col-md-8">
                                    			Derajat sosoh 100%<br>
                                    			Kandungan Air 14%
                                    		</div>
                                    	</div>
                                    	<div class="row">
                                    		<div class="col-md-4">
                                    			<label>Kapasitas Produksi</label>
                                    		</div>
                                    		<div class="col-md-8">
                                    			5 Ton Per Hari
                                    		</div>
                                    	</div>
                                    	<div class="row">
                                    		<div class="col-md-4">
                                    			<label>Jenis Packing</label>
                                    		</div>
                                    		<div class="col-md-8">
                                    			Karung Goni (50kg)
                                    		</div>
                                    	</div>
                                    </div>
                                    <div class="my-4">
                                    	<form>
                                    		<div class="row">
                                    			<div class="col-5 col-sm-4 col-lg-3">
                                    				<div class="form-group">
														<label class="font-weight-bold" for="exampleFormControlInput1">Jumlah</label>
														<div class="input-group mb-3">
															<div class="input-group-prepend">
																<span class="input-group-text">-</span>
															</div>
															<input type="text" class="form-control font-weight-bold text-center" value="5">
															<div class="input-group-append">
																<span class="input-group-text">+</span>
															</div>
														</div>
													</div>
                                    			</div>
                                    			<div class="col-12 col-sm-8 col-lg-9">
													<div class="form-group">
														<label class="font-weight-bold" for="exampleFormControlInput1">Catatan untuk Penjual (Opsional)</label>
														<input type="email" class="form-control rounded-0" placeholder="Contoh : Warna Putih, Ukuran XL, Edisi Ke-2">
													</div>
                                    			</div>
                                    		</div>
                                    	</form>                                    	
                                    </div>
                                    <!-- Produk Action -->
                                        <!-- For Dekstop -->
                                            <div class="d-none d-md-block">
                                            	<div class="row">
                                            		<div class="col-2 col-md-2">
                                            			<span class="btn w-100 btn-like-produk"><i class="icon"></i></span>
                                            		</div>
                                            		<div class="col-5 col-md-5">
                                            			<a href="#" class="btn w-100 py-2">Tambah ke Keranjang</a>
                                            		</div>
                                            		<div class="col-5 col-md-5">
                                            			<a href="#" class="btn btn-orange w-100 py-2">Beli</a>
                                            		</div>
                                            	</div>
                                            </div>    
                                        <!-- End For Dekstop -->
                                        <!-- For Mobile -->
                                            <div class="nav-produk-act-mobile d-md-none">
                                                <div class="container">
                                                    <div class="d-flex">
                                                        <span class="btn w-auto btn-like-produk"><i class="icon"></i></span>
                                                        <a href="#" class="btn w-auto py-2"><i class="fas fa-shopping-cart"></i></a>
                                                        <a href="#" class="btn btn-orange w-100 py-2 flex-grow-1">Beli</a>      
                                                    </div>                                                     
                                                </div>
                                            </div>  
                                        <!-- End For Mobile -->         
                                    <!-- End Produk Action -->                     
								</div>
							</div>
						</div>
					</div>
                    <div class="produk-tab my-5">
                        <ul class="nav nav-pills nav-justified nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#omformasi-produk" role="tab" aria-controls="home" aria-selected="true">Informasi Produk</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#ulasan-pembeli" role="tab" aria-controls="profile" aria-selected="false">Ulasan Pembeli</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="komentar-tab" data-toggle="tab" href="#komentar" role="tab" aria-controls="komentar" aria-selected="false">Komentar</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="omformasi-produk" role="tabpanel" >
                                <div class="card border-0 rounded-0">
                                    <div class="card-body py-4">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="ulasan-pembeli" role="tabpanel"">
                                <div class="card border-0 rounded-0">
                                    <div class="card-body py-4">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="panel panel-white post">
                                                    <div class="post-heading">
                                                        <div class="float-left image">
                                                            <img src="http://bootdey.com/img/Content/user_1.jpg" class="img-circle avatar" alt="user profile image">
                                                        </div>
                                                        <div class="float-left meta">
                                                            <div class="title h5">
                                                                <a href="#"><b>Ryan Haywood</b></a>
                                                                Berkomentar.
                                                            </div>
                                                            <h6 class="text-muted time">1 minute ago</h6>
                                                        </div>
                                                    </div> 
                                                    <div class="post-description"> 
                                                        <p>Bootdey is a gallery of free snippets resources templates and utilities for bootstrap css hmtl js framework. Codes for developers and web designers</p>
                                                        <div class="stats">
                                                            <ul class="rating-list">
                                                                <li class="active"><i class="fas fa-star"></i></li>
                                                                <li class="active"><i class="fas fa-star"></i></li>
                                                                <li class="active"><i class="fas fa-star"></i></li>
                                                                <li class="active"><i class="fas fa-star"></i></li>
                                                                <li class=""><i class="fas fa-star"></i></li>
                                                            </ul>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="panel panel-white post">
                                                    <div class="post-heading">
                                                        <div class="float-left image">
                                                            <img src="http://bootdey.com/img/Content/user_1.jpg" class="img-circle avatar" alt="user profile image">
                                                        </div>
                                                        <div class="float-left meta">
                                                            <div class="title h5">
                                                                <a href="#"><b>Ryan Haywood</b></a>
                                                                Berkomentar.
                                                            </div>
                                                            <h6 class="text-muted time">1 minute ago</h6>
                                                        </div>
                                                    </div> 
                                                    <div class="post-description"> 
                                                        <p>Bootdey is a gallery of free snippets resources templates and utilities for bootstrap css hmtl js framework. Codes for developers and web designers</p>
                                                        <div class="stats">
                                                            <ul class="rating-list">
                                                                <li class="active"><i class="fas fa-star"></i></li>
                                                                <li class="active"><i class="fas fa-star"></i></li>
                                                                <li class="active"><i class="fas fa-star"></i></li>
                                                                <li class="active"><i class="fas fa-star"></i></li>
                                                                <li class=""><i class="fas fa-star"></i></li>
                                                            </ul>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="panel panel-white post">
                                                    <div class="post-heading">
                                                        <div class="float-left image">
                                                            <img src="http://bootdey.com/img/Content/user_1.jpg" class="img-circle avatar" alt="user profile image">
                                                        </div>
                                                        <div class="float-left meta">
                                                            <div class="title h5">
                                                                <a href="#"><b>Ryan Haywood</b></a>
                                                                Berkomentar.
                                                            </div>
                                                            <h6 class="text-muted time">1 minute ago</h6>
                                                        </div>
                                                    </div> 
                                                    <div class="post-description"> 
                                                        <p>Bootdey is a gallery of free snippets resources templates and utilities for bootstrap css hmtl js framework. Codes for developers and web designers</p>
                                                        <div class="stats">
                                                            <ul class="rating-list">
                                                                <li class="active"><i class="fas fa-star"></i></li>
                                                                <li class="active"><i class="fas fa-star"></i></li>
                                                                <li class="active"><i class="fas fa-star"></i></li>
                                                                <li class="active"><i class="fas fa-star"></i></li>
                                                                <li class=""><i class="fas fa-star"></i></li>
                                                            </ul>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <nav>
                                                    <ul class="pagination justify-content-center">
                                                        <li class="page-item disabled">
                                                            <span class="page-link">Previous</span>
                                                        </li>
                                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                        <li class="page-item active">
                                                            <span class="page-link">
                                                                2
                                                                <span class="sr-only">(current)</span>
                                                            </span>
                                                        </li>
                                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                        <li class="page-item">
                                                            <a class="page-link" href="#">Next</a>
                                                        </li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="komentar" role="tabpanel">
                                <div class="card border-0 rounded-0">
                                    <div class="card-body py-4">
                                        <div id="disqus_thread"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" mb-3">
                        <h1 class="pb-2">Rekomendasi Produk</h1>
                        <div class="row justify-content-center mt-4">

                            <?php for ($x = 0; $x <= 3; $x++) { ?>
                                <div class="col-6 col-md-4 col-lg-3">
                                    <a href="#">
                                        <div class="product-list">
                                            <div class="position-relative">
                                                <img src="bwdassets/images/produk-1.png" class="img-produk">
                                                <div class="product-list-content-hover">
                                                    <div class="font-weight-bold">Arduino</div>
                                                    <div class="text-grey-soft">Kualitas : A</div>
                                                    <div class="text-grey-soft">Surabaya</div>
                                                </div>
                                            </div>
                                            <div class="product-list-content py-2 px-3">
                                                <div class="mb-1">
                                                    <ul class="rating-list float-left mb-0 mr-2">
                                                        <li class="active"><i class="fas fa-star"></i></li>
                                                        <li class="active"><i class="fas fa-star"></i></li>
                                                        <li class="active"><i class="fas fa-star"></i></li>                                                        
                                                        <li class=""><i class="fas fa-star"></i></li>
                                                    </ul>
                                                    <small class="text-grey-soft">294 Ulasan</small>
                                                </div> 
                                                <div class="product-list-title mb-1">
                                                    Beras Cianjur
                                                </div>   
                                                <div class="product-list-price mb-1">
                                                    <h3>Rp10rb @ Kg</h3>
                                                </div>                                       
                                            </div>
                                        </div>    
                                    </a>
                                </div>
                            <?php } ?>                    
                            
                        </div>
                    </div>                  
				</div>
                <div class="col-lg-3">
                    <div class="card border-0 rounded-0 mb-3">
                        <div class="card-body">
                            <span class="text-grey-soft text-12">Disediakan & dikirimkan oleh</span>
                            <div class="text-16 my-2">
                                CV. Makmur Jaya Sentosa
                            </div>
                            <div class="d-flex align-items-center">
                                <img src="bwdassets/images/location.png" class="mr-2"><span>Surabaya, Jawa Timur</span>
                            </div>
                        </div>
                    </div>
                    <div class="card border-0 rounded-0 mb-3">
                        <div class="card-body">
                            <span class="font-weight-bold text-16 ">Pengiriman</span>
                            <div class="my-2">
                                Menggunakan Transportasi : <br>
                                Truck Tronton
                            </div>
                            <div class="card-panel">
                                <span class="font-weight-bold">Catatan</span> : Estimasi Biaya Pengiriman maksimal 1 x 24 Jam setelah transaksi berhasil.
                            </div>
                        </div>
                    </div>
                    <div class="iklan">
                        <img src="bwdassets/images/iklan.png" class="">
                    </div>
                </div>
			</div>
		</div>
	</div>

<?php include 'footer.php' ?>

<script type="text/javascript">
$('.produk-detail-slide').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.produk-detail-slide-nav'
});
$('.produk-detail-slide-nav').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    asNavFor: '.produk-detail-slide',
    dots: false,
    centerMode: true,
    focusOnSelect: true
}); 
</script>


<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://ebonni.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
