        <footer class="text-center text-md-left">

            <div class="container py-5">
                <div class="row">
                    <div class="col-md-3">
                        <label>PT. Ebonni Indonesia</label>
                        <p>
                            Jl. Samadi 3 No 17A - Kota Batu, Jawa Timur - Indonesia 65313
                        </p>
                        <p>
                            0341 - 512001 / 087859272001
                        </p>
                    </div>
                    <div class="col-md-3">
                        <label>Tentang Kami</label>
                        <ul class="list-inline">
                            <li>
                                <a href="#">About e-bonni.com</a>
                            </li>
                            <li>
                                <a href="#">Sitemap</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <label>Customer Services</label>
                        <ul class="list-inline">
                            <li>
                                <a href="#"> Help Center</a>
                            </li>
                            <li>
                                <a href="#">Contact Us</a>
                            </li>
                            <li>
                                <a href="#">Report Abuse</a>
                            </li>
                            <li>
                                <a href="#">Submit a Dispute</a>
                            </li>
                            <li>
                                <a href="#">Policies & Rules</a>
                            </li>
                            <li>
                                <a href="#">Get Paid for Your Feedback</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <label>Terhubung</label>
                        <ul class="social-media">
                            <li class="">
                                <a href="#"><i class="fab fa-instagram"></i></a>
                            </li>
                            <li class="">
                                <a href="#"><i class="fab fa-facebook-square"></i></a>
                            </li>
                            <li class="">
                                <a href="#"><i class="fab fa-twitter-square"></i></a>
                            </li>
                            <li class="">
                                <a href="#"><i class="fab fa-google-plus-g"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="container">
                <hr class="my-0">
                <div class="copyright py-3 text-center">
                    <span>© 2018-2020 e-bonni.com. All rights reserved.</span>
                </div>
            </div>
        </footer>

        <div class="return-to-top" id="return-to-top">
            <i class="fas fa-angle-up"></i>
        </div>

        <!-- Login Modal  -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-sm modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header pb-0 border-bottom-0">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body pt-1 pb-5 px-5">
                            <div class="text-center mb-3">
                                <img src="bwdassets/images/logo.png" class="img-fluid">
                            </div>
                            <div class="posisiton-relative float-left w-100 my-3">
                                <b class="float-left">Login</b>
                                <a href="#" class="float-right"><b class="float-left text-orange">Daftar</b></a>
                            </div>

                            <form>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email address">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                </div>     
                                <div>
                                    <a href="#" class="text-orange mt-1">Lupa Password? </a>
                                </div>                           
                                <button type="submit" class="btn btn-yellow font-weight-bold mt-2 float-right">Login</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        <!-- End Login Modal  -->

        <!-- Cart Modal -->
            <div class="modal cart-modal fade" id="cart-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-md" role="document">
                    <div class="modal-content">
                        <div class="modal-header px-4 align-items-center">
                            <h5 class="modal-title" id="exampleModalLabel">Keranjang Belanja</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body px-4 py-4">
                            <div class="card h-100 border-0 rounded-0">

                                <div class="row product-list-item-in">
                                    <div class="col-sm-3 mb-2">
                                        <img src="bwdassets/images/produk-1.png" class="img-fluid w-100">
                                    </div>
                                    <div class="col-sm-3 mb-2">
                                        <div>Beras Cianjur</div>
                                        <div class="text-12">Arduino</div>
                                        <div class="text-12 text-grey">Kualitas : A</div>
                                    </div>
                                    <div class="col-sm-3 mb-2">
                                        <div class="text-orange">Rp10.000 / Kg</div>
                                    </div>
                                    <div class="col-sm-3 mb-2">
                                        <div>
                                            <span class="text-grey">Qty :</span> 5 Ton
                                        </div>
                                        <div>
                                            <a href="#" class="text-blue">Hapus</a>   
                                        </div>
                                        <div>
                                            <a href="#" class="text-blue">Tambah Jumlah</a>
                                        </div>                                        
                                    </div>
                                </div>

                                <div class="row product-list-item-in">
                                    <div class="col-sm-3 mb-2">
                                        <img src="bwdassets/images/produk-1.png" class="img-fluid w-100">
                                    </div>
                                    <div class="col-sm-3 mb-2">
                                        <div>Beras Cianjur</div>
                                        <div class="text-12">Arduino</div>
                                        <div class="text-12 text-grey">Kualitas : A</div>
                                    </div>
                                    <div class="col-sm-3 mb-2">
                                        <div class="text-orange">Rp10.000 / Kg</div>
                                    </div>
                                    <div class="col-sm-3 mb-2">
                                        <div>
                                            <span class="text-grey">Qty :</span> 5 Ton
                                        </div>
                                        <div>
                                            <a href="#" class="text-blue">Hapus</a>   
                                        </div>
                                        <div>
                                            <a href="#" class="text-blue">Tambah Jumlah</a>
                                        </div>    
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="modal-footer d-block">
                            <div class="row">
                                <div class="col-md-6 d-flex align-items-center">
                                    <div class="text-18 text-semi-bold">Total Belanja: <span class="text-orange">Rp130.000.000</span></div>
                                </div>
                                <div class="col-md-6 text-right">
                                    <button type="button" class="btn btn-orange font-weight-bold px-4 text-16">Lanjutkan Pembayaran</button>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        <!-- End Cart Modal -->

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="bwdassets/js/jquery-3.3.1.min.js"></script>
        <script src="bwdassets/js/popper.min.js"></script>
        <script src="bwdassets/js/bootstrap.min.js"></script>
        <script src="bwdassets/js/owl.carousel.min.js"></script>
        <script src="bwdassets/js/slick.min.js"></script>
        <script src="bwdassets/js/custom.js"></script>
        
    </body>
</html>