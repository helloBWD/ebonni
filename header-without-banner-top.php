<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="theme-color" content="#EEF2F5">

        <!-- Style CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
        <link rel="stylesheet" href="bwdassets/css/bwdstyle.css">
        <script type="text/javascript" src="https://cdn.rawgit.com/asvd/dragscroll/master/dragscroll.js"></script>
        <title>Ebonni!</title>

        <style type="text/css">
            /*Custom Header nav background*/
                .header-sec{
                    background-image: url(bwdassets/images/blibli-bg-pattern.png);/* Jika backgorund menggunakan gambar*/
                    /*background-color: #ddd; Jika backgorund menggunakan warna*/
                }
            /*End Custom Header nav background*/

            /*Custom Home Banner*/
                .home-banner{
                    background-color: #8A827E;
                }
            /*End Custom Home Banner*/
        </style>

    </head>
    <body>

        <header>
            <nav class="navbar menu-nav navbar-expand-md py-0">
                <div class="container">
                    <button class="navbar-toggler px-3" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-bars text-white"></i>
                    </button>

                    <div class="collapse px-3 px-md-0 navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto mt-2 mt-md-0">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">MENJADI SELLER</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">GABUNG  SEBAGAI LOGISTIK</a>
                            </li>
                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">CUSTOMER CARE</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">LACAK PESANAN</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="modal" data-target="#exampleModal">LOGIN</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="register.php">DAFTAR</a>
                            </li>
                        </ul>         
                    </div>    
                </div>                    
            </nav>
            <div class="nav-fix-pos">
                <div class="nav-fix">
                    <div class="header-sec">
                        <div class="container">
                            <div class="row">
                                <div class="col-2 d-flex align-items-center">
                                    <img src="bwdassets/images/logo.png" class="img-fluid logo">
                                </div>
                                <div class="col-8 d-flex align-items-center">
                                    <form class="search-form">
                                        <div class="select-kategori">
                                            <div class="dropdown mr-1">
                                                <button type="button" class="btn min-width-auto d-none d-md-block btn-tog-kategori dropdown-toggle px-md-4 ">
                                                    Kategori                                                    
                                                </button>
                                                <div class="btn min-width-auto border-0 d-md-none" >
                                                    <i class="fas fa-list-ul d-md-none btn-tog-kategori"></i>
                                                </div>                                                
                                                <div class="kategori-dropdown">
                                                    <div class="kategori-dropdown-sec">
                                                        <a href="#" class="kategori-dropdown-item">Beras</a>
                                                        <a href="#" class="kategori-dropdown-item">Gula Putih</a>
                                                        <a href="#" class="kategori-dropdown-item">Kopi</a>
                                                        <a href="#" class="kategori-dropdown-item">Sayuran</a>
                                                        <a href="#" class="kategori-dropdown-item">Beras</a>
                                                        <a href="#" class="kategori-dropdown-item">Gula Putih</a>
                                                        <a href="#" class="kategori-dropdown-item">Kopi</a>
                                                        <a href="#" class="kategori-dropdown-item">Sayuran</a>
                                                        <a href="#" class="kategori-dropdown-item">Beras</a>
                                                        <a href="#" class="kategori-dropdown-item">Gula Putih</a>
                                                        <a href="#" class="kategori-dropdown-item">Kopi</a>
                                                        <a href="#" class="kategori-dropdown-item">Sayuran</a>
                                                        <a href="#" class="kategori-dropdown-item">Beras</a>
                                                        <a href="#" class="kategori-dropdown-item">Gula Putih</a>
                                                        <a href="#" class="kategori-dropdown-item">Kopi</a>
                                                        <a href="#" class="kategori-dropdown-item">Sayuran</a>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>                                        
                                        <input type="text" name="search" class="input-search" placeholder="Cari di Ebonni">
                                        <button type="submit" class="btn-search"><img src="bwdassets/images/search-icon.png" class="img-fluid"></button>
                                    </form>
                                </div>
                                <div class="col-2 d-flex align-items-center justify-content-end">
                                    <a href="#" class="" data-toggle="modal" data-target="#cart-modal"><img src="bwdassets/images/shopping-cart-icon.png" class="img-fluid shopping-cart-icon"></a>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </header>