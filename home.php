    <?php include 'header-homepage.php' ?>

        <section class="home-banner">
            <div class="container">
                <div class="kategori-bar my-4">
                    <span class="heading">
                        KATEGORI
                    </span>
                    <div class="kategori-list dragscroll">
                        <ul >
                           <li>
                                <a href="#">Beras</a>
                            </li>
                            <li>
                                <a href="#">Gula</a>
                            </li>
                            <li>
                                <a href="#">Kopi</a>
                            </li>
                            <li>
                                <a href="#">Teh</a>
                            </li>
                            <li>
                                <a href="#">Sayuran</a>
                            </li>
                            <li>
                                <a href="#">Kopi</a>
                            </li>
                            <li>
                                <a href="#">Teh</a>
                            </li>
                            <li>
                                <a href="#">Sayuran</a>
                            </li>
                            <li>
                                <a href="#">Sayuran</a>
                            </li>
                            <li>
                                <a href="#">Kopi</a>
                            </li>
                            <li>
                                <a href="#">Teh</a>
                            </li>
                            <li>
                                <a href="#">Sayuran</a>
                            </li>
                            <li>
                                <a href="#">Teh</a>
                            </li>
                            <li>
                                <a href="#">Sayuran</a>
                            </li>
                            <li>
                                <a href="#">Sayuran</a>
                            </li>
                            <li>
                                <a href="#">Teh</a>
                            </li>
                            <li>
                                <a href="#">Sayuran</a>
                            </li>
                        </ul>    
                    </div>
                    
                </div>
                <div class="row mb-5">
                    <div class="col-md-8 mb-3 mb-md-0">
                        <div class="banner-home-slide owl-carousel owl-theme">
                            <div class="item">
                                <img src="bwdassets/images/banner.png" class="img-fluid">
                            </div>
                            <div class="item">
                                <img src="bwdassets/images/banner.png" class="img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-6 col-md-12 mb-0 mb-md-3">
                                <a href="#" class="">
                                    <img src="bwdassets/images/img1.png" class="img-fluid w-100 ">
                                </a>
                            </div>
                            <div class="col-6 col-md-12 mb-0 mb-md-3">
                                <a href="#" class="">
                                    <img src="bwdassets/images/img2.png" class="img-fluid w-100 ">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="bg-grey-soft">
            <div class="container spacinng-content">
                <div class="row mb-2 ">
                    <div class="col-6">
                        <h2>Rekomendasi Kami</h2>
                    </div>
                    <div class="col-6 d-flex justify-content-end">
                        <a href="#" class="text-orange">Lihat Semua</a>
                    </div>
                </div>
                <div class="row justify-content-center">

                    <?php for ($x = 0; $x <= 7; $x++) { ?>
                        <div class="col-6 col-md-4 col-lg-3">
                            <a href="#">
                                <div class="product-list">
                                    <div class="position-relative">
                                        <img src="bwdassets/images/produk-1.png" class="img-produk">
                                        <div class="product-list-content-hover">
                                            <div class="font-weight-bold">Arduino</div>
                                            <div class="text-grey-soft">Kualitas : A</div>
                                            <div class="text-grey-soft">Surabaya</div>
                                        </div>
                                    </div>
                                    <div class="product-list-content py-2 px-3">
                                        <div class="mb-1">
                                            <ul class="rating-list float-left mb-0 mr-2">
                                                <li class="active"><i class="fas fa-star"></i></li>
                                                <li class="active"><i class="fas fa-star"></i></li>
                                                <li class="active"><i class="fas fa-star"></i></li>
                                                <li class=""><i class="fas fa-star"></i></li>
                                            </ul>
                                            <small class="text-grey-soft">294 Ulasan</small>
                                        </div> 
                                        <div class="product-list-title mb-1">
                                            Beras Cianjur
                                        </div>   
                                        <div class="product-list-price mb-1">
                                            <h3>Rp10rb @ Kg</h3>
                                        </div>                                       
                                    </div>
                                </div>    
                            </a>
                        </div>
                    <?php } ?>                    
                    
                </div>
            </div>
        </section>

        <section class="bg-orange py-4">
            <div class="container">
                <div class="row mb-2">
                    <div class="col-6">
                        <h2 class="text-white">Produk Terlaris</h2>
                    </div>
                    <div class="col-6 d-flex justify-content-end">
                        <a href="#" class="text-white">Lihat Semua</a>
                    </div>
                </div>
                <div class="produk-list-slide owl-carousel owl-theme">
                    <?php for ($x = 0; $x <= 7; $x++) { ?>
                        <div class="item">
                            <a href="#">
                                <div class="product-list">
                                    <div class="position-relative">
                                        <img src="bwdassets/images/produk-1.png" class="img-produk">
                                        <div class="product-list-content-hover">
                                            <div class="font-weight-bold">Arduino</div>
                                            <div class="text-grey-soft">Kualitas : A</div>
                                            <div class="text-grey-soft">Surabaya</div>
                                        </div>
                                    </div>
                                    <div class="product-list-content py-2 px-3">
                                        <div class="mb-1">
                                            <ul class="rating-list float-left mb-0 mr-2">
                                                <li class="active"><i class="fas fa-star"></i></li>
                                                <li class="active"><i class="fas fa-star"></i></li>
                                                <li class="active"><i class="fas fa-star"></i></li>
                                                <li class=""><i class="fas fa-star"></i></li>
                                            </ul>
                                            <small class="text-grey-soft">294 Ulasan</small>
                                        </div> 
                                        <div class="product-list-title mb-1">
                                            Beras Cianjur
                                        </div>   
                                        <div class="product-list-price mb-1">
                                            <h3>Rp10rb @ Kg</h3>
                                        </div>                                       
                                    </div>
                                </div>    
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>

        <section class="bg-grey-soft">
            <div class="container spacinng-content pb-0">
                <div class="row">
                    <div class="col-md-9">
                        <div class="row mb-2 ">
                            <div class="col-6">
                                <h2>Produk Terbanyak Dilihat</h2>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <a href="#" class="text-orange">Lihat Semua</a>
                            </div>
                        </div>
                        <div class="row">
                            <?php for ($x = 0; $x <= 5; $x++) { ?>
                                <div class="col-6 col-md-4 col-lg-4">
                                    <a href="#">
                                        <div class="product-list">
                                            <div class="position-relative">
                                                <img src="bwdassets/images/produk-1.png" class="img-produk">
                                                <div class="product-list-content-hover">
                                                    <div class="font-weight-bold">Arduino</div>
                                                    <div class="text-grey-soft">Kualitas : A</div>
                                                    <div class="text-grey-soft">Surabaya</div>
                                                </div>
                                            </div>
                                            <div class="product-list-content py-2 px-3">
                                                <div class="mb-1">
                                                    <ul class="rating-list float-left mb-0 mr-2">
                                                        <li class="active"><i class="fas fa-star"></i></li>
                                                        <li class="active"><i class="fas fa-star"></i></li>
                                                        <li class="active"><i class="fas fa-star"></i></li>
                                                        <li class=""><i class="fas fa-star"></i></li>
                                                    </ul>
                                                    <small class="text-grey-soft">294 Ulasan</small>
                                                </div> 
                                                <div class="product-list-title mb-1">
                                                    Beras Cianjur
                                                </div>   
                                                <div class="product-list-price mb-1">
                                                    <h3>Rp10rb @ Kg</h3>
                                                </div>                                       
                                            </div>
                                        </div>    
                                    </a>
                                </div>
                            <?php } ?>                              
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="iklan">
                            <img src="bwdassets/images/iklan.png" class="">
                        </div>
                    </div> 
                </div>
            </div>
            <div class="container spacinng-content pb-0">
                <div class="row">                    
                    <div class="col-md-9 order-md-2">
                        <div class="row mb-2 ">
                            <div class="col-6">
                                <h2>Produk Terbanyak Review</h2>
                            </div>
                            <div class="col-6 d-flex justify-content-end">
                                <a href="#" class="text-orange">Lihat Semua</a>
                            </div>
                        </div>
                        <div class="row">
                            <?php for ($x = 0; $x <= 5; $x++) { ?>
                                <div class="col-6 col-md-4 col-lg-4">
                                    <a href="#">
                                        <div class="product-list">
                                            <div class="position-relative">
                                                <img src="bwdassets/images/produk-1.png" class="img-produk">
                                                <div class="product-list-content-hover">
                                                    <div class="font-weight-bold">Arduino</div>
                                                    <div class="text-grey-soft">Kualitas : A</div>
                                                    <div class="text-grey-soft">Surabaya</div>
                                                </div>
                                            </div>
                                            <div class="product-list-content py-2 px-3">
                                                <div class="mb-1">
                                                    <ul class="rating-list float-left mb-0 mr-2">
                                                        <li class="active"><i class="fas fa-star"></i></li>
                                                        <li class="active"><i class="fas fa-star"></i></li>
                                                        <li class="active"><i class="fas fa-star"></i></li>
                                                        <li class=""><i class="fas fa-star"></i></li>
                                                    </ul>
                                                    <small class="text-grey-soft">294 Ulasan</small>
                                                </div> 
                                                <div class="product-list-title mb-1">
                                                    Beras Cianjur
                                                </div>   
                                                <div class="product-list-price mb-1">
                                                    <h3>Rp10rb @ Kg</h3>
                                                </div>                                       
                                            </div>
                                        </div>    
                                    </a>
                                </div>
                            <?php } ?>                              
                        </div>
                    </div>       
                    <div class="col-md-3 order-md-1">
                        <div class="iklan">
                            <img src="bwdassets/images/iklan.png" class="">
                        </div>
                    </div>              
                </div>
            </div>
        </section>

        <section class="bg-grey-soft">
            <div class="container spacinng-content">
                <div class="row mb-2 ">
                    <div class="col-6">
                        <h2>Rekomendasi Kami</h2>
                    </div>
                    <div class="col-6 d-flex justify-content-end">
                        <a href="#" class="text-orange">Lihat Semua</a>
                    </div>
                </div>
                <div class="row">

                    <?php for ($x = 0; $x <= 11; $x++) { ?>
                        <div class="col-6 col-md-4 col-lg-3">
                            <a href="#">
                                <div class="product-list">
                                    <div class="position-relative">
                                        <img src="bwdassets/images/produk-1.png" class="img-produk">
                                        <div class="product-list-content-hover">
                                            <div class="font-weight-bold">Arduino</div>
                                            <div class="text-grey-soft">Kualitas : A</div>
                                            <div class="text-grey-soft">Surabaya</div>
                                        </div>
                                    </div>
                                    <div class="product-list-content py-2 px-3">
                                        <div class="mb-1">
                                            <ul class="rating-list float-left mb-0 mr-2">
                                                <li class="active"><i class="fas fa-star"></i></li>
                                                <li class="active"><i class="fas fa-star"></i></li>
                                                <li class="active"><i class="fas fa-star"></i></li>
                                                <li class=""><i class="fas fa-star"></i></li>
                                            </ul>
                                            <small class="text-grey-soft">294 Ulasan</small>
                                        </div> 
                                        <div class="product-list-title mb-1">
                                            Beras Cianjur
                                        </div>   
                                        <div class="product-list-price mb-1">
                                            <h3>Rp10rb @ Kg</h3>
                                        </div>                                       
                                    </div>
                                </div>    
                            </a>
                        </div>
                    <?php } ?>                    
                    
                </div>
                <div class="row justify-content-center mt-3">
                    <div class="col-8 col-md-4">
                        <a href="#" class="btn-more w-100">MUAT LEBUH BANYAK</a>
                    </div>
                </div>                
            </div>
        </section>

        <section class="bg-grey-soft">
            <div class="container">
                <hr class="my-0">
            </div>
        </section>

        <section class="bg-grey-soft py-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4 mb-3 mb-md-0">
                        <a href=""><img src="bwdassets/images/join.png" class="img-fluid w-100"></a>
                    </div>
                    <div class="col-12 col-md-4 mb-3 mb-md-0">
                        <a href=""><img src="bwdassets/images/join2.png" class="img-fluid w-100"></a>
                    </div>
                    <div class="col-12 col-md-4 mb-3 mb-md-0">
                        <a href=""><img src="bwdassets/images/join3.png" class="img-fluid w-100"></a>
                    </div>
                </div>
            </div>
        </section>

        <section class="py-5">
            <div class="container">
                <div class="text-center">
                    <h2>Our Partner</h2>
                </div>
                <div class="patner owl-carousel owl-theme">
                    <?php for ($x = 0; $x <= 7; $x++) { ?>
                        <div class="item">
                            <img src="bwdassets/images/patner.png">
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>

        <section class="">
            <div class="container">
                <hr class="my-0">
            </div>
        </section>

        <section class="py-4">
            <div class="container">
                <img src="bwdassets/images/iklan2.png" class="img-fluid w-100">
            </div>
        </section>

    <?php include 'footer.php' ?>