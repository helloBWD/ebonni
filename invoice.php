<?php include 'header-without-nav.php' ?>

	<nav class="navbar navbar-light navbar-checkout bg-white">
		<div class="container">
			<a class="navbar-brand" href="#">
				<img src="bwdassets/images/logo-text.png" height="30" alt="">
			</a>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					Invoice
				</li>
			</ul>
		</div>
		
	</nav>

	<div class="py-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8">
					<div class="card rounded-0">
						<div class="card-header">
							<span class="text-16 font-weight-bold">Pembayaran via Transfer</span>
						</div>
						<div class="card-body">
							<div class="text-center">
								Batas pembayaran: <span class="font-weight-bold" id="waktu-pembayaran"></span>
								<div class="py-4">
									<div>
										Jumlah tagihan:
									</div>
									<div class="tagihan">
										Rp13.165.<span>849</span>
									</div>
									<div>
										<span class="salin-jumlah clip" data-clipboard-text="Rp13.165.849">Salin Jumlah</span>
									</div>
									<div>
										<div class="tooltip-box">
											<div class="tooltip-pointer">
												
											</div>
											<div class="tooltip-content">
												Transfer tepat hingga 3 digit terakhir agar
												<br>
												tidak menghambat proses verifikasi
											</div>
										</div>
									</div>									
								</div>
								<div>
									<span>Nomor Tagihan:</span><br>
									<span class="nomor-tagihan text-orange font-weight-bold">BL1811LEK4ZGINV</span>
								</div>
							</div>							
						</div>
						<div class="card-body bg-grey-soft">
							<p>Pembayaran dapat dilakukan ke salah satu rekening berikut:</p>
							<div class="bank-box-sec mb-4">
								<div class="bank-box-item">
									<div class="bank-box">
										<img src="bwdassets/images/logo-bca.gif" class="img-bank">
										<div class="">Bank BCA, Jakarta</div>
										<div class=" font-weight-bold">731 025 2527 </div>
										<div class=" my-2"> <span class="salin-jumlah clip" data-clipboard-text="731 025 2527">Salin No. Rek</span> </div>
									</div>
								</div>
								<div class="bank-box-item">
									<div class="bank-box">
										<img src="bwdassets/images/logo-mandiri.gif" class="img-bank">
										<div class="">Bank Mandiri, Jakarta</div>
										<div class=" font-weight-bold">731 025 2527 </div>
										<div class=" my-2"> <span class="salin-jumlah clip"data-clipboard-text="731 025 2527">Salin No. Rek</span> </div>
									</div>
								</div>
								<div class="bank-box-item">
									<div class="bank-box">
										<img src="bwdassets/images/logo-bsm.gif" class="img-bank">
										<div class="">Bank BSM, Jakarta</div>
										<div class=" font-weight-bold">731 025 2527 </div>
										<div class=" my-2"> <span class="salin-jumlah clip"data-clipboard-text="731 025 2527">Salin No. Rek</span> </div>
									</div>
								</div>
								<div class="bank-box-item">
									<div class="bank-box">
										<img src="bwdassets/images/logo-bni.gif" class="img-bank">
										<div class="">Bank BNI, Jakarta</div>
										<div class=" font-weight-bold">731 025 2527 </div>
										<div class=" my-2"> <span class="salin-jumlah clip"data-clipboard-text="731 025 2527">Salin No. Rek</span> </div>
									</div>
								</div>
								<div class="bank-box-item">
									<div class="bank-box">
										<img src="bwdassets/images/logo-bri.gif" class="img-bank">
										<div class="">Bank BRI, Jakarta</div>
										<div class=" font-weight-bold">731 025 2527 </div>
										<div class=" my-2"> <span class="salin-jumlah clip"data-clipboard-text="731 025 2527">Salin No. Rek</span> </div>
									</div>
								</div>
							</div>
							<p>Pembelianmu dicatat dengan nomor tagihan pembayaran <a href="#" class="text-orange">#BL1811LEK4ZGINV</a>. Bukalapak akan melakukan verifikasi otomatis paling lama 30 menit setelah kamu melakukan pembayaran. Jika kamu menghadapi kendala mengenai pembayaran, silakan langsung <a href="#" class="text-orange">BukaBantuan</a>.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php include 'footer-without-nav.php' ?>
	<script>
		// Set the date we're counting down to
		var countDownDate = new Date("Oct 24, 2018 15:00:00").getTime();

		// Update the count down every 1 second
		var x = setInterval(function() {

		// Get todays date and time
		var now = new Date().getTime();

		// Find the distance between now and the count down date
		var distance = countDownDate - now;

		// Time calculations for days, hours, minutes and seconds
		var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		var seconds = Math.floor((distance % (1000 * 60)) / 1000);

		// Display the result in the element with id="demo"
		document.getElementById("waktu-pembayaran").innerHTML = hours + " jam "
		+ minutes + " menit " + seconds + " detik ";

		// If the count down is finished, write some text 
		if (distance < 0) {
		clearInterval(x);
		document.getElementById("waktu-pembayaran").innerHTML = "EXPIRED";
		}
		}, 1000);
	</script>
	<script src="bwdassets/js/clipboard.min.js"></script>
	<script type="text/javascript">
	// Tooltip

	$('.clip').tooltip({
	  trigger: 'click',
	  placement: 'bottom'
	});

	function setTooltip(btn, message) {
	  $(btn).tooltip('hide')
	    .attr('data-original-title', message)
	    .tooltip('show');
	}

	function hideTooltip(btn) {
	  setTimeout(function() {
	    $(btn).tooltip('hide');
	  }, 1000);
	}

	// Clipboard

	var clipboard = new ClipboardJS('.clip');

	clipboard.on('success', function(e) {
	  setTooltip(e.trigger, 'Copied!');
	  hideTooltip(e.trigger);
	});

	clipboard.on('error', function(e) {
	  setTooltip(e.trigger, 'Failed!');
	  hideTooltip(e.trigger);
	});
	</script>
