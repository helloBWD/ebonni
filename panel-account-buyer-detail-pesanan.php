    <?php include 'header-without-banner-top.php' ?>

    	<div class="page-content">
    		<div class="container">
                <div class="row">
                    <div class="col-md-4 col-lg-3 mb-4">
                        <div>Halo, Alfian Prasetyo</div>
                        <span class="tag tag-green mt-3"><i class="fas fa-check mr-2"></i>Verified Account</span>
                        <div class="card mt-4">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <a href="#">Profil Saya</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">Alamat</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">Pesanan</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">Ulasan</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-8 col-lg-9">
                        <h1 class="mb-3">Detail Pesanan</h1>
                        <div class="row">
                            <div class="col-lg-12 mb-4">
                                <div class="card h-100 border-0 rounded-0">
                                    <div class="card-body py-4 px-5">
                                        <div class="row">
                                            <div class="col-md-5 d-flex align-items-center">
                                                <div class="d-block">
                                                    <div class="font-weight-bold">No. Pesanan : 220415833768590</div>
                                                    <div class="text-12 text-grey">Dipesan pada 25/09/2018</div>    
                                                </div>                                                
                                            </div>
                                            <div class="col-md-7 d-flex align-items-center justify-content-end">
                                                <div class="text-18">
                                                    <span class="text-grey mr-3">Total :</span> Rp59.500.000
                                                </div>
                                            </div>
                                        </div>                               
                                    </div>
                                </div>
                            </div>   
                            <div class="col-lg-12 mb-4">
                                <div class="card h-100 border-0 rounded-0">
                                    <div class="card-header bg-white pt-4 px-5">
                                        <h2 class="mb-0 d-inline-block text-black font-weight-normal">Pengiriman</h2>
                                        <a href="#" class="float-right text-blue text-12">Bantuan</a>
                                    </div>
                                    <div class="card-body py-4 px-5">

                                        <!-- Transaksi Step -->
                                            <!-- Class "completed-steps" untuk step yang sudah selesai -->
                                            <div class="mb-4">
                                                <div class="step-transaksi justify-content-center">
                                                    <span class="step-1 completed-steps"></span>
                                                    <span class="step-2"></span>
                                                    <span class="step-3"></span>
                                                    <span class="step-4"></span>
                                                    <span class="step-5"></span>
                                                </div>
                                            </div>
                                        <!-- End Transaksi Step -->

                                        <div class="mb-3">
                                            Dikirimkan pada 28/09/2018                                            
                                        </div>                        
                                        <div class="panel-grey px-5 py-4">
                                            <div class="row mb-2">
                                                <div class="col-lg-3">
                                                    <span class="text-grey">28 Sep 2018 - 11:25</span>
                                                </div>
                                                <div class="col-lg-9">
                                                    Pesanan anda telah tiba. Terima kasih sudah berbelanja di Ebonni!
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-lg-3">
                                                    <span class="text-grey">28 Sep 2018 - 11:25</span>
                                                </div>
                                                <div class="col-lg-9">
                                                    Pesanan anda akan segera menuju alamat anda. Vendor Logistik kami akan menghubungi anda. 
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-lg-3">
                                                    <span class="text-grey">28 Sep 2018 - 11:25</span>
                                                </div>
                                                <div class="col-lg-9">
                                                    Pesanan anda telah dikirim oleh jasa pengiriman kami dengan nomor LXRP-8622661080.
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-lg-3">
                                                    <span class="text-grey">28 Sep 2018 - 11:25</span>
                                                </div>
                                                <div class="col-lg-9">
                                                    Pesanan Anda sudah diterima dan akan segera dikirim
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-lg-3">
                                                    <span class="text-grey">28 Sep 2018 - 11:25</span>
                                                </div>
                                                <div class="col-lg-9">
                                                    Terima kasih sudah berbelanja di Ebonni! Pesanan anda telah kami terima dan sedang dalam proses verifikasi. Kami akan mengirimkan update selanjutnya ke email anda.
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <div class="col-lg-3">
                                                    <span class="text-grey">28 Sep 2018 - 11:25</span>
                                                </div>
                                                <div class="col-lg-9">
                                                    Pesanan Anda telah berhasil diverifikasi!
                                                </div>
                                            </div>
                                        </div>      
                                    </div>
                                </div>
                            </div> 
                            <div class="col-lg-12 mb-4">
                                <div class="card h-100 border-0 rounded-0">
                                    <div class="card-header bg-white pt-4 px-5">
                                        <h2 class="mb-0 d-inline-block text-black font-weight-normal">Detail Barang</h2>
                                        <a href="#" class="float-right text-blue text-12">Bantuan</a>
                                    </div>
                                    <div class="card-body py-4 px-5">
                                        <div class="row product-list-item-in">
                                            <div class="col-lg-2 mb-2">
                                                <img src="bwdassets/images/produk-1.png" class="img-fluid w-100">
                                            </div>
                                            <div class="col-lg-2 mb-2">
                                                <div>Beras Cianjur</div>
                                                <div class="text-12">Arduino</div>
                                                <div class="text-12 text-grey">Kualitas : A</div>
                                            </div>
                                            <div class="col-lg-2 mb-2 text-lg-center">
                                                <div>Surabaya</div>
                                            </div>
                                            <div class="col-lg-2 mb-2 text-lg-center">
                                                <div>Rp10.000 / Kg</div>
                                            </div>
                                            <div class="col-lg-2 mb-2 text-lg-center">
                                                <span>Qty :</span> 5 Ton
                                            </div>
                                            <div class="col-lg-2 text-lg-right">
                                                <a href="#" class="text-blue">Tulis Ulasan</a>
                                            </div>
                                        </div>
                                        <div class="row product-list-item-in">
                                            <div class="col-lg-2 mb-2">
                                                <img src="bwdassets/images/produk-1.png" class="img-fluid w-100">
                                            </div>
                                            <div class="col-lg-2 mb-2">
                                                <div>Beras Cianjur</div>
                                                <div class="text-12">Arduino</div>
                                                <div class="text-12 text-grey">Kualitas : A</div>
                                            </div>
                                            <div class="col-lg-2 mb-2 text-lg-center">
                                                <div>Surabaya</div>
                                            </div>
                                            <div class="col-lg-2 mb-2 text-lg-center">
                                                <div>Rp10.000 / Kg</div>
                                            </div>
                                            <div class="col-lg-2 mb-2 text-lg-center">
                                                <span>Qty :</span> 5 Ton
                                            </div>
                                            <div class="col-lg-2 text-lg-right">
                                                <a href="#" class="text-blue">Tulis Ulasan</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="col-lg-6 mb-4">
                                <div class="card h-100 border-0 rounded-0">
                                    <div class="card-body py-4 px-5">
                                        <div class="d-flex align-items-center mb-3">
                                            <h2 class="mb-0 d-inline-block text-black">Alamat Pengiriman </h2> 
                                        </div>
                                        <p class="mb-1">Alfian Prasetyo</p>    
                                        <p class="mb-1">65313, Jln. Samadi Gg 3 No. 26, Jawa Timur, Kota Batu, Batum</p>   
                                        <p class="mb-1 mt-2">087859272001</p>                                   
                                    </div>
                                </div>
                            </div>  
                            <div class="col-lg-6 mb-4">
                                <div class="card h-100 border-0 rounded-0">
                                    <div class="card-body py-4 px-5">
                                        <div class="d-flex align-items-center mb-3">
                                            <h2 class="mb-0 d-inline-block text-black">Rincian </h2> 
                                        </div>
                                        <div>
                                            <div class="row">
                                                <div class="col">
                                                    <span>Subtotal</span>
                                                </div>
                                                <div class="col text-right">
                                                    <span>Rp58.000.000</span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <span>Biaya Pengiriman</span>
                                                </div>
                                                <div class="col text-right">
                                                    <span>Rp1.500.000</span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <span class="text-16">Total</span>
                                                </div>
                                                <div class="col text-right">
                                                    <span class="text-16">Rp59.500.000</span>
                                                </div>
                                            </div>
                                        </div>                                 
                                    </div>
                                </div>
                            </div>            
                        </div>
                    </div>    
                </div>
    		</div>
    	</div>

    <?php include 'footer.php' ?>