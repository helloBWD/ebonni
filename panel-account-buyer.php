    <?php include 'header-without-banner-top.php' ?>

    	<div class="page-content">
    		<div class="container">
                <div class="row">
                    <div class="col-md-4 col-lg-3 mb-4">
                        <div>Halo, Alfian Prasetyo</div>
                        <span class="tag tag-green mt-3"><i class="fas fa-check mr-2"></i>Verified Account</span>
                        <div class="card mt-4">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <a href="#">Profil Saya</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">Alamat</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">Pesanan</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">Ulasan</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-8 col-lg-9">
                        <h1 class="mb-3">Panel Akun</h1>
                        <div class="row">
                            <div class="col-lg-6 mb-4">
                                <div class="card h-100 border-0 rounded-0">
                                    <div class="card-body py-4 px-5">
                                        <div class="d-flex align-items-center mb-3">
                                            <h2 class="mb-0 d-inline-block text-black text-border-right">Profil</h2> <a href="#" class="text-12 text-blue">UBAH</a>
                                        </div>
                                        <p class="mb-1">Alfian Prasetyo</p>    
                                        <p class="mb-1">alfianprast14@gmail.com</p>   
                                        <p class="mb-1">087859272001</p>                                   
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-4">
                                <div class="card h-100 border-0 rounded-0">
                                    <div class="card-body py-4 px-5">
                                        <div class="d-flex align-items-center mb-3">
                                            <h2 class="mb-0 d-inline-block text-black text-border-right">Profil</h2> <a href="#" class="text-12 text-blue">Alamat</a>
                                        </div>
                                        <p class="mb-1 font-weight-bold">Alfian Prasetyo</p>    
                                        <p class="mb-1">
                                            Jln. Samadi Gg 3 No. 26<br>
                                            Jawa Timur - Kota Batu - Batu<br>
                                            (+62) 087859272001
                                        </p>                                    
                                    </div>
                                </div>
                            </div> 
                            <div class="col-lg-12 mb-4">
                                <div class="card h-100 border-0 rounded-0">
                                    <div class="card-body py-4 px-5">
                                        <div class="d-flex align-items-center mb-3">
                                            <h2 class="mb-0 d-inline-block text-black">Pesanan Terakhir</h2>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table text-12 mt-2">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th scope="col">No. Pesanan</th>
                                                        <th scope="col">Di pesan pada</th>
                                                        <th scope="col">Barang</th>
                                                        <th scope="col">Total</th>
                                                        <th scope="col">#</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th>220415833768590</th>
                                                        <td>25/09/2018</td>
                                                        <td>Beras Cianjur</td>
                                                        <td>Rp100.000.000</td>
                                                        <td> <a href="#" class="text-blue">DETAIL</a> </td>
                                                    </tr>
                                                    <tr>
                                                        <th>220415833768590</th>
                                                        <td>25/09/2018</td>
                                                        <td>Beras Cianjur</td>
                                                        <td>Rp100.000.000</td>
                                                        <td> <a href="#" class="text-blue">DETAIL</a> </td>
                                                    </tr>
                                                </tbody>
                                            </table> 
                                        </div>                                
                                    </div>
                                </div>
                            </div>                       
                        </div>
                    </div>    
                </div>
    		</div>
    	</div>

    <?php include 'footer.php' ?>