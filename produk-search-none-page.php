	<?php include 'header-without-banner-top.php' ?>

		<div class="page-content">
			<div class="container">
				<div class="row">
					<div class="col-md-9 order-md-2">
						<div class="row">
							<div class="col-6 col-md-7 col-lg-8 mb-3 mb-sm-0 d-flex align-items-center ">
								<div class="d-block">
									<span class="text-17">Pencarian : </span><span class="text-17 font-weight-bold ml-1">Beras</span>
									<div><span class="text-12 text-grey-soft">200 Ditemukan</span></div>
								</div>								
							</div>
							<div class="col-6 col-md-5 col-lg-4 d-flex align-items-center">
								<span class="font-weight-bold mr-2">Urutkan&nbsp;:</span>
								<form class="w-100">
									<div class="form-group mb-0 ">
										<select class="form-control text-12 rounded-0" >
											<option>Harga Terendah</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>
								</form>							
							</div>
						</div>
						<hr class="mt-4 mb-5">
						<div class="produk-list-sec mb-5">
							<div class="row mt-4 justify-content-center">
								
			                    <div class="col-12 h-100">
			                    	<div class="produk-none-cont text-center">
			                    		<i class="fas fa-box-open text-orange icon"></i>
			                    		<div class="text">
			                    			Maaf barang yang anda cari tidak ada...
			                    		</div>
			                    	</div>
			                    </div>

				            </div>
		                </div>
					</div>
					<div class="col-md-3 order-md-1">
						<div class="card rounded-0 mb-3">
                            <div class="card-header">
                            	<div class="d-flex justify-content-between">
                            		<span class="text-13">Filter</span>
                            		<a href="#" class="text-orange text-12">Reset</a>
                            	</div>                            	
                            </div>
							<ul class="list-group list-group-flush">
								<li class="list-group-item">
									<div class="input-group">
										<input type="text" class="form-control rounded-0" placeholder="Cari">
										<div class="input-group-append">
											<a href="#" class="input-group-text rounded-0" id="basic-addon2"><i class="fas fa-search"></i></a>
										</div>
									</div>
								</li>
								<li class="list-group-item">									
									<label class="font-weight-bold text-13 mb-0">Kategori</label>
									<div class="my-2">
										<div class="form-check">
											<input class="form-check-input" type="checkbox" value="" id="check1">
											<label class="form-check-label text-12" for="check1">
												Beras
											</label>
										</div>
										<div class="form-check">
											<input class="form-check-input" type="checkbox" value="" id="check2">
											<label class="form-check-label text-12" for="check2">
												Gula
											</label>
										</div>
										<div class="form-check">
											<input class="form-check-input" type="checkbox" value="" id="check3">
											<label class="form-check-label text-12" for="check3">
												Kopi
											</label>
										</div>
										<div class="form-check">
											<input class="form-check-input" type="checkbox" value="" id="check4">
											<label class="form-check-label text-12" for="check4">
												Teh
											</label>
										</div>
									</div>
									<a href="#" class="text-12 text-orange">+ Kategori Lainnya</a>
								</li>
								<li class="list-group-item">
									<label class="font-weight-bold text-13 mb-0">Lokasi Penjual</label>
									<select class="form-control rounded-0 mt-2" id="exampleFormControlSelect1">
										<option>Pilih Lokasi</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select>
								</li>
								<li class="list-group-item">
									<label class="font-weight-bold text-13 mb-0">Penjual</label>
									<div class="my-2">
										<div class="form-check">
											<input class="form-check-input" type="checkbox" value="" id="check1">
											<label class="form-check-label text-12" for="check1">
												Premium
											</label>
										</div>
									</div>
								</li>
								<li class="list-group-item">
									<label class="font-weight-bold text-13 mb-2">Harga</label>
									<div class="input-group mb-2">
										<div class="input-group-prepend ">
											<div class="input-group-text rounded-0 bg-transparent border-right-0">Rp</div>
										</div>
										<input type="text" class="form-control rounded-0 border-left-0 text-right" placeholder="Minimum">
									</div>
									<div class="input-group mb-2">
										<div class="input-group-prepend ">
											<div class="input-group-text rounded-0 bg-transparent border-right-0">Rp</div>
										</div>
										<input type="text" class="form-control rounded-0 border-left-0 text-right" placeholder="Maksimum">
									</div>
								</li>
								<li class="list-group-item">
									<a href="#" class="btn btn-orange w-100 rounded-0 font-weight-bold py-2 my-2">Tampilkan</a>
								</li>
							</ul>
                        </div>
					</div>					
				</div>
			</div>
		</div>

    <?php include 'footer.php' ?>