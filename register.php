<?php include 'header-without-nav.php' ?>

    	<div class="register py-5">
    		<div class="container">
    			<div class="text-center">
    				<img src="bwdassets/images/logo-text.png" class="img-fluid mb-3">
    				<h3>Daftar akun baru sekarang</h3>
    			</div>
    			<div class="row justify-content-center mt-4">
    				<div class="col-10 col-md-8 col-lg-5">
		    			<form>
							<div class="form-group">
								<input type="text" class="form-control invalid" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Lengkap">
								<div class="invalid-feedback">
									Nama lengkap harus diisi
								</div>
							</div>
							<div class="form-group">
								<input type="email" class="form-control" placeholder="Email">
							</div>
							<div class="form-group">
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
									<label class="custom-control-label" for="customRadioInline1">Laki - Laki</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
									<label class="custom-control-label" for="customRadioInline2">Perempuan</label>
								</div>
							</div>
							<div class="form-group">
								<input type="password" class="form-control" placeholder="Password">
							</div>
							<div class="form-group">
								<input type="password" class="form-control" placeholder="Ketik Ulang Password">
							</div>
							<div class="form-group">
								<input type="numer" class="form-control" placeholder="Nomor Handphone">
							</div>

							<div class="form-group text-12">
								Dengan klik daftar, kamu telah menyetujui <a href="" class="text-orange">Aturan Penggunaan</a> dan <a href="" class="text-orange">Kebijakan Privasi</a> dari Ebonni
							</div>

							<button type="submit" class="btn btn-orange w-100 mb-3">Daftar</button>

							<div class="form-group text-12">
								<span>Sudah punya akun? <a href="" class="text-orange">Silakan login</a></span>
								<a href="" class="text-orange float-right">Bantuan</a>
							</div>
						</form>		
    				</div>
    			</div>
    			
    		</div>
    	</div>

<?php include 'footer-without-nav.php' ?>